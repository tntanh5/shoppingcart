﻿var myApp = angular.module('myApp', ['ngRoute', 'angularUtils.directives.dirPagination', 'ngCart', 'ngAnimate', 'ui.bootstrap']);
myApp.config(function ($routeProvider) {
    $routeProvider.
        when('/', { templateUrl: 'Views/Home/Home.html', controller: 'HomeController' }).
        when('/category/:IdCategory', { templateUrl: 'Views/BookCategory.html', controller: 'BookCategoryController' }).
        when('/author/:IdAuthor', { templateUrl: 'Views/BookAuthor.html', controller: 'BookAuthorController' }).
        when('/publishingcompany/:IdPubCom', { templateUrl: 'Views/BookPubCom.html', controller: 'BookPubComController' }).
        when('/detailbook/:IdCategory/:IdBook', { templateUrl: 'Views/DetailBook.html', controller: 'DetailBookController' }).
        when('/cart', { templateUrl: 'Views/Cart.html'}).
        
        otherwise({ redirectTo: '/' });

});

var MONGOLAB_API_KEY = '1SaoW3P1uPgoG1uspep_0l-2V5oAKyxc';
myApp.controller('HomeController', function ($scope, $http, $window) {

    //alert('fdfaf');
    $http({
        method: 'GET',
        url: 'https://api.mongolab.com/api/1/databases/bookshopdb/collections/books?apiKey=' + MONGOLAB_API_KEY
    })
    .success(function (data, status) {
        $scope.products = data;
    })
    .error(function (data, status) {
        alert(status);
    });
    $scope.pageSize = 10;
    $scope.options = [{
        name: 'Hiển thị',
        value: 10
    }, {
        name: '5',
        value: 5
    },
    {
        name: '10',
        value: 10
    },
    {
        name: '15',
        value: 15
    }];

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        //$scope.reverse = b; //if true make it false and vice versa
    }

    $scope.onSelect = function ($item, $model, $label) {
        $scope.$item = $item;
        $scope.$model = $model;
        $scope.$label = $label;
        //console.log($scope.$item);
        //console.log($scope.$model);
        //console.log($scope.$label);
        //$log.info($scope.$item);
        //$log.info($scope.$model);
        //$log.info($scope.$label);
        //window.location.href= '/detail/' + $scope.$model.IdCategory+'/'+$scope.$model.IdBook;
        var url = 'http://' + $window.location.host + '/#/detailbook/' + $scope.$model.IdCategory + '/' + $scope.$model.IdBook;
        //console.log(url);
        $window.location.href = url;
    }
}
);
myApp.controller('CategoryController', function ($scope, $http) {
    $http({
        method: 'GET',
        url: 'https://api.mongolab.com/api/1/databases/bookshopdb/collections/category?apiKey=' + MONGOLAB_API_KEY

    })
            .success(function (data, status) {

                $scope.categories = data;
            })
            .error(function (data, status) {
                alert(status);
            });

    

}
);
myApp.controller('PublishingCompanyController', function ($scope, $http) {
    $http({
        method: 'GET',
        url: 'https://api.mongolab.com/api/1/databases/bookshopdb/collections/publishingCompany?apiKey=' + MONGOLAB_API_KEY

    })
            .success(function (data, status) {

                $scope.companies = data;
            })
            .error(function (data, status) {
                alert(status);
            });

    //$scope.pageSize = 10;

    //$scope.sort = function (keyname) {
    //    $scope.sortKey = keyname;   //set the sortKey to the param passed
    //    //$scope.reverse = b; //if true make it false and vice versa
    //}
}
);
myApp.controller('AuthorController', function ($scope, $http) {
    $http({
        method: 'GET',
        url: 'https://api.mongolab.com/api/1/databases/bookshopdb/collections/author?apiKey=' + MONGOLAB_API_KEY

    })
            .success(function (data, status) {

                $scope.authors = data;
            })
            .error(function (data, status) {
                alert(status);
            });

    //$scope.pageSize = 10;

    //$scope.sort = function (keyname) {
    //    $scope.sortKey = keyname;   //set the sortKey to the param passed
    //    //$scope.reverse = b; //if true make it false and vice versa
    //}
}
);


